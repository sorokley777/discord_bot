import disnake
from disnake.ext import commands

TOKEN = "" # <- введите ваш токен в ковычки. 
bot = commands.InteractionBot(intents=disnake.Intents.all())

@bot.event
async def on_ready():
    print("Bot is ready!")

@bot.slash_command(name="ping", description="Ping command")
async def ping_cmd(inter: disnake.CommandInteraction):
    await inter.response.send_message("Pong!")


if __name__ == "__main__":
    bot.run(TOKEN)